# Meta Entity

## Description


Meta Entity module allows to add metadata about an entity, stored in a
dedicated entity (`meta_entity`). This is useful when you want to avoid storing
this information as a content entity field and clutter the content entity with
data that is not part of the content but is [metadata
](https://en.wikipedia.org/wiki/Metadata) (data about other data).

## Requirements

* [Dynamic Entity
Reference](https://www.drupal.org/project/dynamic_entity_reference) module.

## How to use it?

Even the module provides a user interface, it's mainly intended to be used
programmatically. Meta data is represented by entities, of type `meta_entity`
that are linking content entities. Meta entity types can be defined as bundle
configuration entities (of type `meta_entity_type`) in order to define types of
metadata (e.g. visits count, download count, etc). Each `meta_type` entity is
linked to a content entity (such as node, taxonomy term, etc) via a [Dynamic
Entity Reference](https://www.drupal.org/project/dynamic_entity_reference)
field. A content entity can be referred by a single meta entity of that type.
When defining a meta entity type, the site builder can choose the bundles of
content entities that can be linked by that specific type of metadata.

### Choosing Dynamic Entity Reference version

The Dynamic Entity Reference module has 2 major versions:

* 3.x: Supports only entity types with numeric entity IDs. Has a stable release.
* 4.x: Supports all entity types, including ones that use string IDs. Currently
  in alpha.

It is recommended to use Dynamic Entity Reference 3.x because it is stable and
works with recent versions of MariaDB and MySQL. However if you want to use the
module to provide metadata for entities that use string IDs (e.g. Webforms) then
choose Dynamic Entity Reference 4.x.

For more information see
<a href="https://www.drupal.org/project/dynamic_entity_reference">the Dynamic
Entity Reference project page</a>.


## Reverse references

On each bundle selected for a given meta entity bundle, the site builder may
configure a field name. The target entity will automatically expose an
`entity_reference` computed field with that name, linking back to the meta
entity.

## API usage

Create a meta entity type to be used on 'article' nodes:

```php
MetaEntityType::create([
  'id' => 'visits',
  'label' => 'Visits',
  'mapping' => [
    // Node entity type ID.
    'node' => [
      // The array is keyed by bundle ID. The values are configuration arrays
      // containing:
      // - field_name: (optional) A field identifier string may be configured
      //   as value. In this example the 'article' nodes will automatically
      //   expose a field named `visits` which is a reference to the node's
      //   meta entity.
      // - auto_create: (optional) A boolean value indicating whether to
      //   automatically create a 'visits' meta entity when an 'article' node
      //   is created.
      'article' => [
        'field_name' => 'visits',
        'auto_create' => TRUE,
      ],
      // If the configuration array is empty, no computed field is created and
      // the no meta entity is created when a 'page' node is created but it's
      // still allowed to create meta entities for 'page' nodes.
      'page' => [],
    ],
    // Entity types without bundles, such as 'user', should use the entity type
    // ID as bundle ID.
    'user' => [
      'user' => [
        ...
      ],
    ],
  ],
])->save();
```

Add all needed fields to this meta entity type, either as config entities or
using the UI. For instance, in the above example you could add a configurable
field `field_count` of type `integer`.

Create an 'article' node.
```php
$node = Node::create([
  'type' => 'article',
  'title' => 'News From The World',
  ...
]);
$node->save();
```

Create and link the meta entity:

```php
$visits = MetaEntity::create([
  'type' => 'visits',
  'target' => $node,
  'field_count' => 10,
]);
$visits->save();
```

Now the node `$node` has metadata, represented by a meta entity of type
`visits`, associated without the node to be touched. The meta entity can update
(e.g. increment the `field_count`) without needing to re-save the `$node`. On
meta entity update, also the `$node` cache tags are invalidated.

The content entity can refer directly the meta entity, via the computed reverse
reference:

```php
$message = t('@count visits', [
  '@count' => $node->visits->entity->field_count->value;
]);
```

For this reason, creating an entity with its meta entity could be achieved in a
single step:

```php
Node::create([
  'type' => 'article',
  'title' => 'News From The World',
  'visits' => MetaEntity::create([
    'type' => 'visits',
    'field_count' => 10,
  ]),
])->save();
```

Supposing a new meta entity type, `downloads`, is added and a new metadata
entity of this type is linked to `$node`:

```php
MetaEntityType::create([
  'id' => 'downloads',
  'label' => 'Downloads',
  'mapping' => [
    'node' => [
      'article' => [
        'field_name' => 'download_count',
      ],
    ],
  ],
])->save();

$downloads = MetaEntity::create([
  'type' => 'downloads',
  'target' => $node,
  'field_count' => 100,
]);
$downloads->save();
```

Now there are two reverse reference fields exposed on the node:

```php
// Will return $downloads.
$node->download_count->entity;
// Will return $visits.
$node->visits->entity;
```
