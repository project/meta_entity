<?php

/**
 * @file
 * Meta entity module main functionality.
 */

declare(strict_types=1);

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Render\Element;
use Drupal\meta_entity\Entity\MetaEntityInterface;
use Drupal\meta_entity\MetaEntityReverseReferenceItemList;

/**
 * Implements hook_theme().
 */
function meta_entity_theme(): array {
  return [
    'meta_entity' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Implements template_preprocess_HOOK() for meta_entity.
 */
function template_preprocess_meta_entity(array &$variables): void {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['meta_entity'] = $variables['elements']['#meta_entity'];
  $meta_entity = $variables['meta_entity'];

  $variables['url'] = !$meta_entity->isNew() ? $meta_entity->toUrl('canonical')->toString() : NULL;
  $variables['label'] = !empty($variables['elements']['label']) ? $variables['elements']['label'] : [
    '#markup' => $meta_entity->label(),
  ];

  $variables += ['content' => []];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Implements hook_entity_delete().
 *
 * Deletes all related meta entities when a content entity is deleted.
 */
function meta_entity_entity_delete(EntityInterface $entity): void {
  if (!$entity instanceof ContentEntityInterface) {
    return;
  }

  $entity_type_manager = \Drupal::entityTypeManager();
  foreach (\Drupal::getContainer()->getParameter('meta_entity.repositories') as $meta_entity_type_id => $service_id) {
    /** @var \Drupal\meta_entity\MetaEntityRepositoryInterface $repository */
    $repository = \Drupal::service($service_id);
    if ($meta_entities = $repository->getMetaEntitiesForEntity($entity)) {
      $entity_type_manager->getStorage($meta_entity_type_id)->delete($meta_entities);
    }
  }
}

/**
 * Implements hook_entity_presave().
 *
 * Sets a standard label if there's no current label. Uses the generic
 * hook_entity_presave() instead of the more specific hook_ENTITY_TYPE_presave()
 * in order to allow other entities that implement the MetaEntityInterface have
 * the label automatically set.
 */
function meta_entity_entity_presave(EntityInterface $meta_entity): void {
  if (!$meta_entity instanceof MetaEntityInterface) {
    return;
  }
  // If the meta entity has already a label we'll not overwrite it. Also, don't
  // compute a label if the target entity is not set.
  if (!empty($meta_entity->label()) || $meta_entity->get('target')->isEmpty()) {
    return;
  }

  /** @var \Drupal\Core\Entity\ContentEntityInterface $target_entity */
  if ($target_entity = $meta_entity->get('target')->entity) {
    $target_entity_singular_label = $target_entity->getEntityType()->getSingularLabel();
    $bundle_label = $meta_entity->get('type')->entity->label();
    $meta_entity->label->value = (string) t('@entity @type (@bundle)', [
      '@bundle' => $bundle_label,
      '@entity' => $target_entity->label(),
      '@type' => $target_entity_singular_label,
    ]);
  }
}

/**
 * Implements hook_entity_bundle_field_info().
 *
 * If configured, all content entities with metadata will expose a computed
 * entity reference field, linking back to metadata entity. The name of the
 * field is configurable in the meta entity type level.
 */
function meta_entity_entity_bundle_field_info(EntityTypeInterface $entity_type, string $bundle, array $base_field_definitions): array {
  /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info */
  $bundle_info = \Drupal::service('entity_type.bundle.info');
  $entity_type_id = $entity_type->id();

  $fields = [];
  foreach (\Drupal::getContainer()->getParameter('meta_entity.repositories') as $meta_entity_type_id => $service_id) {
    /** @var \Drupal\meta_entity\MetaEntityRepositoryInterface $repository */
    $repository = \Drupal::service($service_id);
    foreach ($repository->getReverseReferenceFieldNames($entity_type_id, $bundle) as $meta_entity_bundle => $field_name) {
      /** @var \Drupal\Core\Field\BaseFieldDefinition $definition */
      $bundle_label = $bundle_info->getBundleInfo($meta_entity_type_id)[$meta_entity_bundle]['label'];
      $fields[$field_name] = BaseFieldDefinition::create('entity_reference')
        ->setName($field_name)
        ->setTargetEntityTypeId($entity_type_id)
        ->setTargetBundle($bundle)
        ->setLabel(t('@label reference', ['@label' => $bundle_label]))
        ->setDescription(t('@label attached metadata.'))
        ->setSetting('target_type', $meta_entity_type_id)
        ->setSetting('meta_entity_type_id', $meta_entity_bundle)
        ->setCardinality(1)
        ->setDisplayConfigurable('view', TRUE)
        ->setComputed(TRUE)
        ->setClass(MetaEntityReverseReferenceItemList::class);
    }
  }
  return $fields;
}

/**
 * Implements hook_entity_insert().
 */
function meta_entity_entity_insert(EntityInterface $entity) {
  if (!$entity instanceof ContentEntityInterface) {
    return;
  }

  $entity_type_manager = \Drupal::entityTypeManager();
  foreach (\Drupal::getContainer()->getParameter('meta_entity.repositories') as $meta_entity_type_id => $service_id) {
    /** @var \Drupal\meta_entity\MetaEntityRepositoryInterface $repository */
    $repository = \Drupal::service($service_id);
    $meta_entity_bundles = $repository->getTypesWithAutoCreation($entity);
    if ($meta_entity_bundles) {
      $storage = $entity_type_manager->getStorage($meta_entity_type_id);
      foreach ($meta_entity_bundles as $meta_entity_bundle) {
        $meta_entity = $storage->create([
          'type' => $meta_entity_bundle,
          'target' => $entity,
        ]);
        $meta_entity->save();
        \Drupal::logger('meta_entity')->debug("Auto-created %meta_entity_bundle @meta_entity_type_id with ID @meta_entity_id for the %bundle %entity_type entity with ID @entity_id.", [
          '%meta_entity_bundle' => $meta_entity_bundle,
          '@meta_entity_type_id' => str_replace('_', '-', $meta_entity_type_id),
          '@meta_entity_id' => $meta_entity->id(),
          '%bundle' => $entity->bundle(),
          '%entity_type' => $entity->getEntityTypeId(),
          '@entity_id' => $entity->id(),
        ]);
      }
    }
  }
}
