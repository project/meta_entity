<?php

declare(strict_types=1);

namespace Drupal\meta_entity\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the meta entity content entity class.
 *
 * @ContentEntityType(
 *   id = "meta_entity",
 *   label = @Translation("Meta entity"),
 *   label_collection = @Translation("Meta entities"),
 *   bundle_label = @Translation("Meta entity type"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\meta_entity\Form\MetaEntityForm",
 *       "add" = "Drupal\meta_entity\Form\MetaEntityForm",
 *       "edit" = "Drupal\meta_entity\Form\MetaEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "list_builder" = "Drupal\meta_entity\MetaEntityListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\meta_entity\MetaEntityAccessControlHandler",
 *   },
 *   base_table = "meta_entity",
 *   admin_permission = "administer meta entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/content/meta-entity/add/{meta_entity_type}",
 *     "add-page" = "/admin/content/meta-entity/add",
 *     "canonical" = "/meta-entity/{meta_entity}",
 *     "edit-form" = "/admin/content/meta-entity/{meta_entity}/edit",
 *     "delete-form" = "/admin/content/meta-entity/{meta_entity}/delete",
 *     "collection" = "/admin/content/meta-entity"
 *   },
 *   bundle_entity_type = "meta_entity_type",
 *   field_ui_base_route = "entity.meta_entity_type.edit_form",
 * )
 */
class MetaEntity extends ContentEntityBase implements MetaEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 512)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['target'] = BaseFieldDefinition::create('dynamic_entity_reference')
      ->setLabel(t('Target entity'))
      ->setDescription(t('The entity this meta entity is about.'))
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->addConstraint('UniquePerMetaTypeAndTarget', [
        'metaEntityTypeId' => $entity_type->id(),
      ])
      ->addConstraint('MappedTargetEntity', [
        'metaEntityTypeId' => $entity_type->id(),
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the meta entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the meta entity was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 22,
      ])->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): MetaEntityInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntity(): ?ContentEntityInterface {
    return $this->get('target')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function loadOrCreate(string $meta_entity_bundle, ContentEntityInterface $target_entity): MetaEntityInterface {
    return self::loadOrCreateHelper('meta_entity', $meta_entity_bundle, $target_entity);
  }

  /**
   * Helper class to load or create the meta entity.
   *
   * @param string $meta_entity_type_id
   *   The meta entity type ID to be loaded or created.
   * @param string $meta_entity_bundle
   *   The meta entity type bundle to be loaded or created.
   * @param \Drupal\Core\Entity\ContentEntityInterface $target_entity
   *   The target content entity.
   *
   * @return \Drupal\meta_entity\Entity\MetaEntityInterface
   *   A meta entity.
   */
  protected static function loadOrCreateHelper(string $meta_entity_type_id, string $meta_entity_bundle, ContentEntityInterface $target_entity): MetaEntityInterface {
    $storage = \Drupal::entityTypeManager()->getStorage($meta_entity_type_id);
    $ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', $meta_entity_bundle)
      ->condition('target.target_type', $target_entity->getEntityTypeId())
      ->condition('target.target_id', $target_entity->id())
      ->execute();

    if ($ids) {
      $id = reset($ids);
      $meta_entity = $storage->load($id);
    }
    else {
      $meta_entity = $storage->create([
        'type' => $meta_entity_bundle,
        'target' => $target_entity,
      ]);
    }

    /** @var \Drupal\meta_entity\Entity\MetaEntityInterface $meta_entity */
    return $meta_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate(): array {
    return Cache::mergeTags(
      parent::getCacheTagsToInvalidate(),
      $this->getTargetEntityCacheTagsToInvalidate(),
      $this->cacheTags
    );
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateTagsOnSave($update): void {
    parent::invalidateTagsOnSave($update);
    // We should invalidate target entity cache; regardless if the meta entity
    // is new, or it was just updated.
    Cache::invalidateTags($this->getTargetEntityCacheTagsToInvalidate());
  }

  /**
   * Returns the cache tags of the target entity to invalidate.
   *
   * @return string[]
   *   The cache tags of the target entity to invalidate.
   */
  protected function getTargetEntityCacheTagsToInvalidate(): array {
    if (!$target_entity = $this->getTargetEntity()) {
      return [];
    }
    return $target_entity->getCacheTagsToInvalidate();
  }

}
