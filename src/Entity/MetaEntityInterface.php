<?php

declare(strict_types=1);

namespace Drupal\meta_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for 'meta_entity' entities.
 */
interface MetaEntityInterface extends ContentEntityInterface {

  /**
   * Gets the meta entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the meta entity.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the meta entity creation timestamp.
   *
   * @param int $timestamp
   *   The meta entity creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp): self;

  /**
   * Return the entity this meta entity refers to.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The entity this meta entity refers to.
   */
  public function getTargetEntity(): ?ContentEntityInterface;

  /**
   * Loads or, if doesn't exists, creates a meta entity given a target entity.
   *
   * @param string $meta_entity_bundle
   *   The meta entity type bundle to be loaded or created.
   * @param \Drupal\Core\Entity\ContentEntityInterface $target_entity
   *   The target content entity.
   *
   * @return \Drupal\meta_entity\Entity\MetaEntityInterface
   *   A meta entity.
   */
  public static function loadOrCreate(string $meta_entity_bundle, ContentEntityInterface $target_entity): MetaEntityInterface;

}
