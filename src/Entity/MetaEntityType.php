<?php

declare(strict_types=1);

namespace Drupal\meta_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the Meta Entity Type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "meta_entity_type",
 *   label = @Translation("Meta entity type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\meta_entity\Form\MetaEntityTypeForm",
 *       "edit" = "Drupal\meta_entity\Form\MetaEntityTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "\Drupal\meta_entity\MetaEntityTypeListBuilder",
 *   },
 *   admin_permission = "administer meta entity",
 *   bundle_of = "meta_entity",
 *   config_prefix = "type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/meta-entity/add",
 *     "edit-form" = "/admin/structure/meta-entity/manage/{meta_entity_type}",
 *     "delete-form" = "/admin/structure/meta-entity/manage/{meta_entity_type}/delete",
 *     "collection" = "/admin/structure/meta-entity",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "mapping",
 *   },
 * )
 */
class MetaEntityType extends ConfigEntityBundleBase implements MetaEntityTypeInterface {

  /**
   * The entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the meta entity type.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the meta entity type.
   *
   * @var string
   */
  protected $description;

  /**
   * The target entity mapping for this meta entity type.
   *
   * @var array
   */
  protected $mapping = [];

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): MetaEntityTypeInterface {
    parent::calculateDependencies();

    $entity_type_manager = \Drupal::entityTypeManager();
    foreach ($this->get('mapping') as $entity_type_id => $bundles) {
      $entity_type = $entity_type_manager->getDefinition($entity_type_id);

      // Add the module that provided the entity type.
      $this->addDependency('module', $entity_type->getProvider());

      // This entity type doesn't define bundles as config entities.
      if (!$bundle_entity_type_id = $entity_type->getBundleEntityType()) {
        continue;
      }
      $bundle_entity_storage = $entity_type_manager->getStorage($bundle_entity_type_id);

      // Add bundle config entities as dependencies.
      foreach ($bundle_entity_storage->loadMultiple(array_keys($bundles)) as $bundle) {
        $this->addDependency($bundle->getConfigDependencyKey(), $bundle->getConfigDependencyName());
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies): bool {
    $changed = parent::onDependencyRemoval($dependencies);

    $removed_config_names = array_keys($dependencies['config']);

    $entity_type_manager = \Drupal::entityTypeManager();
    $mapping = $this->get('mapping');
    foreach ($mapping as $entity_type_id => $bundles) {
      $entity_type = $entity_type_manager->getDefinition($entity_type_id);
      if (in_array($entity_type->getProvider(), $dependencies['module'])) {
        unset($mapping[$entity_type_id]);
        continue;
      }

      // After this if(...) block it's only about removed config dependencies.
      if (!$removed_config_names) {
        continue;
      }

      // This entity type doesn't define bundles as config entities.
      if (!$bundle_entity_type_id = $entity_type->getBundleEntityType()) {
        continue;
      }
      $bundle_entity_storage = $entity_type_manager->getStorage($bundle_entity_type_id);

      // Build an array of config dependency names keyed by config IDs.
      $bundle_ids = array_keys($bundles);
      $config_names = array_map(
        function (ConfigEntityInterface $bundle): string {
          return $bundle->getConfigDependencyName();
        },
        $bundle_entity_storage->loadMultiple($bundle_ids)
      );
      if ($dependencies_to_remove = array_intersect($config_names, $removed_config_names)) {
        $bundle_ids = array_diff($bundle_ids, array_keys($dependencies_to_remove));
        // If all bundle IDs were removed, remove the whole entity type.
        if (!$bundle_ids) {
          unset($mapping[$entity_type_id]);
          continue;
        }
        // Otherwise, set the new bundle IDs list.
        $mapping[$entity_type_id] = array_intersect_key($bundles, array_flip($bundle_ids));
      }
    }

    // The mapping has been changed.
    if ($mapping !== $this->get('mapping')) {
      // Update the property and signal that the config entity should be saved.
      $this->set('mapping', $mapping);
      $changed = TRUE;
    }

    return $changed;
  }

}
