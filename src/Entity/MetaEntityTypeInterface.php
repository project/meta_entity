<?php

declare(strict_types=1);

namespace Drupal\meta_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for 'meta_entity_type' entities.
 */
interface MetaEntityTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the meta entity type description.
   *
   * @return string|null
   *   The description.
   */
  public function getDescription(): ?string;

}
