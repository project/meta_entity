<?php

declare(strict_types=1);

namespace Drupal\meta_entity;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides permissions for meta entities.
 */
class MetaEntityPermissionProvider implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Constructs a new permissions provider instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param string $entityTypeId
   *   The meta entity type ID.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected string $entityTypeId,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager'),
      'meta_entity_type'
    );
  }

  /**
   * Returns a list of permissions.
   *
   * @return array
   *   A list of permissions.
   */
  public function getPermissions(): array {
    $storage = $this->entityTypeManager->getStorage($this->entityTypeId);
    $permissions = [];
    foreach ($storage->loadMultiple() as $meta_entity_type_id => $meta_entity_type) {
      $meta_entity_id = str_replace('_', '-', $meta_entity_type->getEntityType()->getBundleOf());
      $args = [
        '%bundle' => $meta_entity_type->label(),
        '%type' => $meta_entity_id,
      ];
      $permissions += [
        "create {$meta_entity_type_id} {$meta_entity_id}" => [
          'title' => $this->t("%bundle: Create new %type", $args),
        ],
        "view {$meta_entity_type_id} {$meta_entity_id}" => [
          'title' => $this->t("%bundle: View %type", $args),
        ],
        "update {$meta_entity_type_id} {$meta_entity_id}" => [
          'title' => $this->t("%bundle: Edit %type", $args),
        ],
        "delete {$meta_entity_type_id} {$meta_entity_id}" => [
          'title' => $this->t("%bundle: Delete %type", $args),
        ],
      ];
    }
    return $permissions;
  }

}
