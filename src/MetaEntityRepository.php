<?php

declare(strict_types=1);

namespace Drupal\meta_entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\meta_entity\Entity\MetaEntityInterface;

/**
 * Provides a default implementation for 'meta_entity.repository' service.
 */
class MetaEntityRepository implements MetaEntityRepositoryInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cache chained backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The meta entity, entity type ID.
   *
   * @var string
   */
  protected $metaEntityTypeId;

  /**
   * {@inheritdoc}
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): MetaEntityRepositoryInterface {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setMetaEntityTypeId(string $meta_entity_type_id): MetaEntityRepositoryInterface {
    $this->metaEntityTypeId = $meta_entity_type_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCacheBackend(CacheBackendInterface $cache): MetaEntityRepositoryInterface {
    $this->cache = $cache;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetaEntityForEntity(ContentEntityInterface $target_entity, string $meta_entity_bundle): ?MetaEntityInterface {
    foreach ($this->getMetaEntitiesForEntity($target_entity) as $meta_entity) {
      if ($meta_entity->bundle() === $meta_entity_bundle) {
        return $meta_entity;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetaEntitiesForEntity(ContentEntityInterface $target_entity): array {
    if ($target_entity->isNew() || !$this->getMetaEntityTypesForBundle($target_entity->getEntityTypeId(), $target_entity->bundle())) {
      return [];
    }

    $cid = "meta:{$target_entity->getEntityTypeId()}:{$target_entity->id()}";

    // Try to retrieve from cache.
    if ($cache = $this->cache->get($cid)) {
      return $cache->data;
    }

    $storage = $this->entityTypeManager->getStorage($this->metaEntityTypeId);
    $ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('target.target_type', $target_entity->getEntityTypeId())
      ->condition('target.target_id', $target_entity->id())
      // Make the order predictable.
      ->sort($storage->getEntityType()->getKey('bundle'))
      ->execute();
    $meta_entities = $ids ? $storage->loadMultiple($ids) : [];

    // Cache the results. First, tag the cache with the list tags of each meta
    // entity type that is able to link this target entity. As new meta entity
    // types might be added, tag also with list tag of the meta entity type.
    $meta_entity_bundle_entity_type_id = $storage->getEntityType()->getBundleEntityType();
    $cache_tags = $this->entityTypeManager->getDefinition($meta_entity_bundle_entity_type_id)->getListCacheTags();
    foreach ($this->getMetaEntityTypesForBundle($target_entity->getEntityTypeId(), $target_entity->bundle()) as $meta_entity_type_id) {
      $cache_tags = Cache::mergeTags($cache_tags, [$this->metaEntityTypeId . "_list:{$meta_entity_type_id}"]);
    }
    // Finally, tag with the tags of each meta entity.
    array_walk($meta_entities, function (MetaEntityInterface $meta_entity) use (&$cache_tags): void {
      $cache_tags = Cache::mergeTags($cache_tags, $meta_entity->getCacheTags());
    });
    $this->cache->set($cid, $meta_entities, Cache::PERMANENT, $cache_tags);

    return $meta_entities;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetaEntityTypesForBundle(string $target_entity_type_id, ?string $target_bundle = NULL): array {
    $target_bundle = $target_bundle ?: $target_entity_type_id;
    $cid = "types:{$target_entity_type_id}:{$target_bundle}";

    if ($cache = $this->cache->get($cid)) {
      return $cache->data;
    }

    $meta_types = [];
    $meta_entity_bundle_entity_type_id = $this->entityTypeManager->getDefinition($this->metaEntityTypeId)->getBundleEntityType();
    $cache_tags = $this->entityTypeManager->getDefinition($meta_entity_bundle_entity_type_id)->getListCacheTags();
    $storage = $this->entityTypeManager->getStorage($meta_entity_bundle_entity_type_id);
    foreach ($storage->loadMultiple() as $id => $type) {
      $cache_tags = Cache::mergeTags($cache_tags, $type->getCacheTags());
      $mapping = $type->get('mapping');
      if (array_key_exists($target_entity_type_id, $mapping)) {
        if (array_key_exists($target_bundle, $mapping[$target_entity_type_id])) {
          $meta_types[] = $id;
        }
      }
    }

    if ($meta_types) {
      sort($meta_types);
    }

    // Cache the results.
    $this->cache->set($cid, $meta_types, Cache::PERMANENT, $cache_tags);

    return $meta_types;
  }

  /**
   * {@inheritdoc}
   */
  public function getReverseReferenceFieldNames(string $target_entity_type_id, string $target_bundle): array {
    $cid = "fields:{$target_entity_type_id}:{$target_bundle}";

    if ($cache = $this->cache->get($cid)) {
      return $cache->data;
    }

    $fields = [];
    $meta_entity_bundle_entity_type_id = $this->entityTypeManager->getStorage($this->metaEntityTypeId)->getEntityType()->getBundleEntityType();
    $cache_tags = $this->entityTypeManager->getDefinition($meta_entity_bundle_entity_type_id)->getListCacheTags();
    $storage = $this->entityTypeManager->getStorage($meta_entity_bundle_entity_type_id);
    foreach ($storage->loadMultiple() as $id => $type) {
      $cache_tags = Cache::mergeTags($cache_tags, $type->getCacheTags());
      $mapping = $type->get('mapping');
      if (!empty($mapping[$target_entity_type_id][$target_bundle]['field_name'])) {
        $fields[$id] = $mapping[$target_entity_type_id][$target_bundle]['field_name'];
      }
    }

    if ($fields) {
      // Make returned list predictable.
      ksort($fields);
    }

    // Cache the results.
    $this->cache->set($cid, $fields, Cache::PERMANENT, $cache_tags);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getTypesWithAutoCreation(ContentEntityInterface $target_entity): array {
    $cid = 'auto_create';

    if ($cache = $this->cache->get($cid)) {
      if (isset($cache->data[$target_entity->getEntityTypeId()][$target_entity->bundle()])) {
        return $cache->data[$target_entity->getEntityTypeId()][$target_entity->bundle()];
      }
      return [];
    }

    $cache = [];
    $meta_entity_bundle_entity_type_id = $this->entityTypeManager->getStorage($this->metaEntityTypeId)->getEntityType()->getBundleEntityType();
    $storage = $this->entityTypeManager->getStorage($meta_entity_bundle_entity_type_id);
    $cache_tags = $this->entityTypeManager->getDefinition($meta_entity_bundle_entity_type_id)->getListCacheTags();
    foreach ($storage->loadMultiple() as $meta_entity_type_id => $meta_entity_type) {
      $cache_tags = Cache::mergeTags($cache_tags, $meta_entity_type->getCacheTags());
      foreach ($meta_entity_type->get('mapping') as $entity_type_id => $bundles) {
        foreach ($bundles as $bundle_id => $info) {
          $auto_create = $info['auto_create'] ?? FALSE;
          if ($auto_create) {
            $cache[$entity_type_id][$bundle_id][] = $meta_entity_type_id;
          }
        }
      }
    }

    // Cache the results.
    $this->cache->set($cid, $cache, Cache::PERMANENT, $cache_tags);

    if (isset($cache[$target_entity->getEntityTypeId()][$target_entity->bundle()])) {
      return $cache[$target_entity->getEntityTypeId()][$target_entity->bundle()];
    }
    return [];
  }

}
