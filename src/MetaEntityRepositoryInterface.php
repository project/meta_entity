<?php

declare(strict_types=1);

namespace Drupal\meta_entity;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\meta_entity\Entity\MetaEntityInterface;

/**
 * Provides an interface for 'meta_entity.repository' service.
 */
interface MetaEntityRepositoryInterface {

  /**
   * Sets the entity type manager service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): self;

  /**
   * Sets the meta-entity entity type ID.
   *
   * @param string $meta_entity_type_id
   *   The meta-entity entity type ID.
   *
   * @return $this
   */
  public function setMetaEntityTypeId(string $meta_entity_type_id): self;

  /**
   * Sets the cache chained backend.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache chained backend.
   *
   * @return $this
   */
  public function setCacheBackend(CacheBackendInterface $cache): self;

  /**
   * Returns a list of meta entities of a given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $target_entity
   *   The entity to checked.
   * @param string $meta_entity_bundle
   *   The meta entity bundle ID.
   *
   * @return \Drupal\meta_entity\Entity\MetaEntityInterface|null
   *   The meta entity or NULL if it doesn't exist.
   */
  public function getMetaEntityForEntity(ContentEntityInterface $target_entity, string $meta_entity_bundle): ?MetaEntityInterface;

  /**
   * Returns a list of existing meta entities of a given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $target_entity
   *   The entity to checked.
   *
   * @return \Drupal\meta_entity\Entity\MetaEntityInterface[]
   *   Associative array keyed by meta entity ID with meta entity as value.
   */
  public function getMetaEntitiesForEntity(ContentEntityInterface $target_entity): array;

  /**
   * Returns a list of meta entity types available for a given entity bundle.
   *
   * @param string $target_entity_type_id
   *   The entity type ID.
   * @param string|null $target_bundle
   *   The entity bundle. Can be omitted when an entity type without bundles,
   *   such as 'user', is passed.
   *
   * @return string[]
   *   A list of meta entity type IDs.
   */
  public function getMetaEntityTypesForBundle(string $target_entity_type_id, ?string $target_bundle = NULL): array;

  /**
   * Returns a list of reverse reference field names for a given entity bundle.
   *
   * @param string $target_entity_type_id
   *   The entity type ID.
   * @param string $target_bundle
   *   The entity bundle.
   *
   * @return string[]
   *   An associative array, keyed by bundle ID, of field names.
   */
  public function getReverseReferenceFieldNames(string $target_entity_type_id, string $target_bundle): array;

  /**
   * Returns a list of meta entity types that are configured with auto-creation.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $target_entity
   *   The entity for which to return the list.
   *
   * @return string[]
   *   A list of meta entity type IDs.
   */
  public function getTypesWithAutoCreation(ContentEntityInterface $target_entity): array;

}
