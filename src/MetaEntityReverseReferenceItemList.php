<?php

declare(strict_types=1);

namespace Drupal\meta_entity;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Provides a field item list class for the reverse reference computed fields.
 */
class MetaEntityReverseReferenceItemList extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue(): void {
    $entity = $this->getEntity();

    foreach (\Drupal::getContainer()->getParameter('meta_entity.repositories') as $meta_entity_type_id => $service_id) {
      if ($this->getSetting('target_type') === $meta_entity_type_id) {
        /** @var \Drupal\meta_entity\MetaEntityRepositoryInterface $repository */
        $repository = \Drupal::service($service_id);
        if ($meta_entity = $repository->getMetaEntityForEntity($entity, $this->getSetting('meta_entity_type_id'))) {
          $this->list[0] = $this->createItem(0, $meta_entity);
          return;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(): void {
    $target_entity = $this->getEntity();
    foreach ($this->list as $item) {
      /** @var \Drupal\meta_entity\Entity\MetaEntityInterface $meta_entity */
      if (($meta_entity = $item->entity) && $meta_entity->isNew()) {
        if (!empty($target_entity->id())) {
          // The target entity is a new entity with an ID already set on
          // creation. Assign the 'target' field.
          $meta_entity->get('target')->setValue([
            'target_type' => $target_entity->getEntityTypeId(),
            'target_id' => $target_entity->id(),
          ]);
        }
      }
    }
    parent::preSave();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update): bool {
    foreach ($this->list as $item) {
      /** @var \Drupal\meta_entity\Entity\MetaEntityInterface $meta_entity */
      if (($meta_entity = $item->entity) && $meta_entity->get('target')->isEmpty()) {
        // Now that the parent ID exists, let's set the meta entity target.
        // @todo Sadly, we have to save once again the meta entity as it seems
        //   that there's no way to get the target entity ID just before the
        //   meta entity is saved. Investigate this more.
        $meta_entity->set('target', $this->getEntity())->save();
      }
    }
    return parent::postSave($update);
  }

}
