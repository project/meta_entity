<?php

declare(strict_types=1);

namespace Drupal\meta_entity;

use Drupal\Core\Cache\BackendChain;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Stores meta entity repository services IDs as container parameter.
 */
class MetaEntityServiceProvider implements ServiceProviderInterface, ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    foreach ($container->findTaggedServiceIds('meta_entity.repository') as $service_id => $attributes) {
      // Create a cache chained backend for each repository service.
      $container->register("cache.$service_id.persistent", CacheBackendInterface::class)
        ->setFactory([new Reference('cache_factory'), 'get'])
        ->setArgument(0, $service_id)
        ->addTag('cache.bin');
      $container->register("cache.$service_id.memory", MemoryCacheInterface::class)
        ->setFactory([new Reference('cache.backend.memory'), 'get'])
        ->setArgument(0, $attributes[0]['meta_entity_type']);
      $container->register("cache.$service_id.cache", BackendChain::class)
        ->addMethodCall('appendBackend', [new Reference("cache.$service_id.memory")])
        ->addMethodCall('appendBackend', [new Reference("cache.$service_id.persistent")])
        // This tag ensures that Drupal's cache_tags.invalidator service
        // invalidates also this cache data.
        ->addTag('cache.bin');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    $services = [];
    foreach ($container->findTaggedServiceIds('meta_entity.repository') as $service_id => $attributes) {
      $container->getDefinition($service_id)
        ->setClass(MetaEntityRepository::class)
        ->addMethodCall('setEntityTypeManager', [new Reference('entity_type.manager')])
        ->addMethodCall('setMetaEntityTypeId', [$attributes[0]['meta_entity_type']])
        ->addMethodCall('setCacheBackend', [new Reference("cache.$service_id.cache")]);
      $services[$attributes[0]['meta_entity_type']] = $service_id;
    }
    $container->setParameter('meta_entity.repositories', $services);
  }

}
