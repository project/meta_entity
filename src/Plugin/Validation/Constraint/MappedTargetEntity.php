<?php

declare(strict_types=1);

namespace Drupal\meta_entity\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a specific entity can be mapped to a target meta data.
 *
 * @Constraint(
 *   id = "MappedTargetEntity",
 *   label = @Translation("Mapped target entity", context = "Validation"),
 * )
 */
class MappedTargetEntity extends Constraint {

  use MetaEntityConstraintTrait;

  /**
   * Violation message.
   *
   * @var string
   */
  public $message = "%type (%bundle) doesn't qualify to attach %meta meta data.";

}
