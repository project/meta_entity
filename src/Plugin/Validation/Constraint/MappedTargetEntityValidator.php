<?php

declare(strict_types=1);

namespace Drupal\meta_entity\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the target entity type and bundle.
 *
 * The target entity type and bundle should match the ones that are defined in
 * the mapping for this meta entity type.
 */
class MappedTargetEntityValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Constructs a new validator instance.
   *
   * @param \Drupal\meta_entity\MetaEntityRepositoryInterface[] $metaEntityRepositories
   *   The meta entity repositories.
   */
  public function __construct(
    protected readonly array $metaEntityRepositories,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $repository_ids = $container->getParameter('meta_entity.repositories');
    $repositories = array_map([$container, 'get'], $repository_ids);
    return new static($repositories);
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint): void {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $target_entity */
    $target_entity = $items->entity;
    if (empty($target_entity)) {
      return;
    }

    /** @var \Drupal\meta_entity\Plugin\Validation\Constraint\MappedTargetEntity $constraint */
    $repository = $this->metaEntityRepositories[$constraint->metaEntityTypeId];
    $entity_types = $repository->getMetaEntityTypesForBundle($target_entity->getEntityTypeId(), $target_entity->bundle());
    if (!in_array($items->getEntity()->bundle(), $entity_types)) {
      $this->context->buildViolation($constraint->message, [
        '%type' => $target_entity->getEntityType()->getLabel(),
        '%bundle' => $target_entity->bundle(),
        '%meta' => $items->getEntity()->get('type')->entity->label(),
      ])->setCode('MappedTargetEntity')
        ->addViolation();
    }
  }

}
