<?php

declare(strict_types=1);

namespace Drupal\meta_entity\Plugin\Validation\Constraint;

/**
 * Reusable code for meta entity constraints.
 */
trait MetaEntityConstraintTrait {

  /**
   * The meta entity type ID.
   *
   * @var string
   */
  public $metaEntityTypeId;

  /**
   * {@inheritdoc}
   */
  public function getDefaultOption(): string {
    return 'metaEntityTypeId';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredOptions(): array {
    return ['metaEntityTypeId'];
  }

}
