<?php

declare(strict_types=1);

namespace Drupal\meta_entity\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a specific entity is already attached to target meta data.
 *
 * @Constraint(
 *   id = "UniquePerMetaTypeAndTarget",
 *   label = @Translation("Per-meta-type unique target", context = "Validation"),
 * )
 */
class UniquePerMetaTypeAndTarget extends Constraint {

  use MetaEntityConstraintTrait;

  /**
   * Violation message.
   *
   * @var string
   */
  public $message = "%entity @type already has attached %meta meta data.";

}
