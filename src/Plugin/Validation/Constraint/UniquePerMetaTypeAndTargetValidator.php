<?php

declare(strict_types=1);

namespace Drupal\meta_entity\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\meta_entity\Entity\MetaEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniquePerMetaTypeAndTarget constraint.
 */
class UniquePerMetaTypeAndTargetValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * Constructs a new UniquePerMetaTypeAndTargetValidator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint): void {
    /** @var \Drupal\dynamic_entity_reference\Plugin\Field\FieldType\DynamicEntityReferenceFieldItemList $items */
    $meta_entity = $items->getEntity();
    /** @var \Drupal\Core\Entity\EntityInterface $meta_entity */
    if ($items->getFieldDefinition()->getName() !== 'target' || !$meta_entity instanceof MetaEntityInterface) {
      throw new \InvalidArgumentException("This constraint should be used only on field 'target' of an 'meta_entity' entity.");
    }

    /** @var \Drupal\meta_entity\Plugin\Validation\Constraint\UniquePerMetaTypeAndTarget  $constraint */
    $query = $this->entityTypeManager->getStorage($constraint->metaEntityTypeId)->getQuery()->accessCheck(FALSE);
    $entity_id = $meta_entity->id();

    // Using isset() instead of !empty() as 0 and '0' are valid ID values for
    // entity types using string IDs.
    if (isset($entity_id)) {
      $query->condition('id', $entity_id, '<>');
    }

    $has_meta_entity = (bool) $query
      ->condition('type', $meta_entity->bundle())
      ->condition('target.target_type', $items->target_type)
      ->condition('target.target_id', $items->target_id)
      ->range(0, 1)
      ->count()
      ->execute();

    if ($has_meta_entity) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $target_entity */
      $target_entity = $items->entity;
      $this->context->buildViolation($constraint->message, [
        '%entity' => $target_entity->label(),
        '@type' => $target_entity->getEntityType()->getSingularLabel(),
        '%meta' => $meta_entity->get('type')->entity->label(),
      ])->setCode('UniquePerMetaTypeAndTarget')
        ->addViolation();
    }
  }

}
