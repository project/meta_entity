<?php

declare(strict_types=1);

namespace Drupal\Tests\meta_entity\Functional;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\meta_entity\Entity\MetaEntity;
use Drupal\meta_entity\Entity\MetaEntityType;
use Drupal\Tests\system\Functional\Entity\EntityWithUriCacheTagsTestBase;
use Drupal\user\RoleInterface;

/**
 * Tests the Meta entity's cache tags.
 *
 * @group meta_entity
 */
class MetaEntityCacheTagsTest extends EntityWithUriCacheTagsTestBase {

  /**
   * The meta entity.
   *
   * @var \Drupal\meta_entity\Entity\MetaEntityInterface
   */
  protected $entity;

  /**
   * An arbitrary entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $arbitraryEntity;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'meta_entity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Give anonymous users permission to access the meta entity, so that we can
    // verify the cache tags of cached versions of meta entity.
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, ['administer meta entity']);

    $this->arbitraryEntity = EntityTest::create([
      'type' => 'entity_test',
      'name' => 'Arbitrary',
    ]);
    $this->arbitraryEntity->save();
    // Store the cache tags of this entity.
    // @see meta_entity_test_meta_entity_presave()
    // @see meta_entity_test_meta_entity_view()
    \Drupal::state()->set('meta_entity_test.cache_tags', $this->arbitraryEntity->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function createEntity() {
    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => ['entity_test' => []],
    ])->save();

    $target_entity = EntityTest::create([
      'type' => 'entity_test',
      'name' => 'Innocent target',
    ]);
    $target_entity->save();

    $meta_entity = MetaEntity::create([
      'type' => 'visit_count',
      'target' => $target_entity,
    ]);
    $meta_entity->save();

    return $meta_entity;
  }

  /**
   * Tests target entity cache tags invalidation when updating the meta entity.
   */
  public function testTargetEntityCacheClear(): void {
    $target_entity = $this->entity->getTargetEntity();

    // Cache the target entity render array.
    $this->verifyPageCache($target_entity->toUrl(), 'MISS');
    $this->verifyPageCache($target_entity->toUrl(), 'HIT');
    // Warm also the arbitrary entity cache.
    $this->verifyPageCache($this->arbitraryEntity->toUrl(), 'MISS');
    $this->verifyPageCache($this->arbitraryEntity->toUrl(), 'HIT');

    // Update the meta entity.
    $this->entity->set('count', 20)->save();

    // Check that target entity cache tags were invalidated.
    $this->verifyPageCache($target_entity->toUrl(), 'MISS');
    $this->verifyPageCache($target_entity->toUrl(), 'HIT');
    // Check that arbitrary entity cache tags were invalidated too.
    $this->verifyPageCache($this->arbitraryEntity->toUrl(), 'MISS');
    $this->verifyPageCache($this->arbitraryEntity->toUrl(), 'HIT');
  }

  /**
   * Tests target entity cache tag invalidation with a new meta entity.
   *
   * When a meta entity is created for a target entity that already exists, we
   * should invalidate the caches of both the target entity and the meta entity.
   * This ensures that when we are rendering metadata inside the target entity
   * that it will be up-to-date.
   */
  public function testTargetEntityCacheClearOnNew(): void {
    $new_entity = $this->entityValidateAndSave(EntityTest::create([
      'id' => 10,
      'name' => 'Test',
    ]));

    // Cache the target entity render array.
    $this->verifyPageCache($new_entity->toUrl(), 'MISS');
    $this->verifyPageCache($new_entity->toUrl(), 'HIT');

    $meta = MetaEntity::loadOrCreate('visit_count', $new_entity);
    $this->assertTrue($meta->isNew());
    $meta = $this->entityValidateAndSave($meta->set('count', 0));

    $this->verifyPageCache($new_entity->toUrl(), 'MISS');
    $this->verifyPageCache($new_entity->toUrl(), 'HIT');

    $this->assertSame(
      [
        'meta_entity:2',
        'entity_test:10',
      ],
      $meta->getCacheTagsToInvalidate()
    );
    $meta_list_tags_ref = (new \ReflectionClass($meta))
      ->getMethod('getListCacheTagsToInvalidate');
    $meta_list_tags_ref->setAccessible(TRUE);
    $this->assertSame(
      [
        'meta_entity_list',
        'meta_entity_list:visit_count',
      ],
      $meta_list_tags_ref->invoke($meta)
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getAdditionalCacheTagsForEntity(EntityInterface $entity) {
    $tags = parent::getAdditionalCacheTagsForEntity($entity);
    /** @var \Drupal\meta_entity\Entity\MetaEntityInterface $entity */
    $tags = Cache::mergeTags($tags, $entity->getTargetEntity()->getCacheTags());
    // Add also an arbitrary cache tag.
    return Cache::mergeTags($tags, $this->arbitraryEntity->getCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  protected function getAdditionalCacheContextsForEntity(EntityInterface $entity) {
    return Cache::mergeContexts(
      parent::getAdditionalCacheContextsForEntity($entity),
      ['timezone']
    );
  }

  /**
   * Validates, saves and reloads an entity from its storage.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to validate, save and reload.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity, freshly loaded from the storage.
   */
  protected function entityValidateAndSave(ContentEntityInterface $entity): ContentEntityInterface {
    $this->assertEmpty($entity->validate());
    $entity->save();
    $storage = $this->container->get('entity_type.manager')
      ->getStorage($entity->getEntityTypeId());
    $this->assertInstanceOf(ContentEntityStorageInterface::class, $storage);
    $storage->resetCache([$entity->id()]);
    $entity = $storage->loadUnchanged($entity->id());
    $this->assertInstanceOf(ContentEntityInterface::class, $entity);
    return $entity;
  }

}
