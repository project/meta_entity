<?php

declare(strict_types=1);

namespace Drupal\Tests\meta_entity\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\meta_entity\Entity\MetaEntityType;

/**
 * Tests meta entity auto-creation and auto-deletion.
 *
 * @group meta_entity
 */
class AutoCreationTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dynamic_entity_reference',
    'entity_test',
    'field',
    'meta_entity',
    'meta_entity_test',
    'user',
  ];

  /**
   * Tests meta entity auto-creation.
   */
  public function testAutoCreation(): void {
    $this->installConfig(['meta_entity_test']);
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('meta_entity');
    $this->installEntitySchema('user');
    $repository = $this->container->get('meta_entity.repository');

    $meta_entity_type = MetaEntityType::load('visit_count');
    // Set the mapping for but don't set any 'auto-*' flag.
    $meta_entity_type->set('mapping', [
      'entity_test' => [
        'entity_test' => [],
      ],
    ])->save();

    $target_entity = EntityTest::create([
      'type' => 'entity_test',
      'name' => $this->randomString(),
    ]);
    $target_entity->save();

    // Check that no associated meta entity has been created.
    $this->assertNull($repository->getMetaEntityForEntity($target_entity, 'visit_count'));

    $target_entity->delete();

    // Add meta entity auto-creation for this bundle.
    $meta_entity_type->set('mapping', [
      'entity_test' => [
        'entity_test' => [
          'auto_create' => TRUE,
        ],
      ],
    ])->save();

    $target_entity = EntityTest::create([
      'type' => 'entity_test',
      'name' => $this->randomString(),
    ]);
    $target_entity->save();

    // Check that an associated meta entity has been created.
    $meta_entity = $repository->getMetaEntityForEntity($target_entity, 'visit_count');
    $this->assertSame('visit_count', $meta_entity->bundle());

    // Check that the associated meta entity has been deleted.
    $target_entity->delete();
    $this->assertNull($repository->getMetaEntityForEntity($target_entity, 'visit_count'));
  }

}
