<?php

declare(strict_types=1);

namespace Drupal\Tests\meta_entity\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\meta_entity\Entity\MetaEntity;
use Drupal\meta_entity\Entity\MetaEntityType;

/**
 * Tests the 'MappedTargetEntity' constraint.
 *
 * @group meta_entity
 * @coversDefaultClass \Drupal\meta_entity\Plugin\Validation\Constraint\MappedTargetEntityValidator
 */
class MappedTargetEntityConstraintTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dynamic_entity_reference',
    'entity_test',
    'field',
    'meta_entity',
    'meta_entity_test',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('meta_entity');
    $this->installConfig(['meta_entity_test']);
  }

  /**
   * @covers ::validate
   */
  public function testConstraint(): void {
    $entity = EntityTest::create([
      'type' => 'entity_test',
      'name' => 'Just an innocent target',
    ]);
    $entity->save();

    // The target entity is required. Check that a violation is thrown when it
    // is missing.
    $meta_entity = MetaEntity::create([
      'type' => 'visit_count',
      'count' => 30,
      'target' => NULL,
    ]);
    $violations = $meta_entity->validate();
    $this->assertCount(1, $violations);

    $message = strip_tags((string) $violations[0]->getMessage());
    $this->assertSame('This value should not be null.', $message);

    // On 'visit_count' meta entity type there's no mapping set for entity_test.
    $meta_entity = MetaEntity::create([
      'type' => 'visit_count',
      'count' => 30,
      'target' => $entity,
    ]);
    $violations = $meta_entity->validate();
    $this->assertCount(1, $violations);

    $message = strip_tags((string) $violations[0]->getMessage());
    $this->assertSame("Test entity (entity_test) doesn't qualify to attach Visit count meta data.", $message);

    // Allow entity_test:entity_test to attach metadata to the "download_count"
    // meta entity type. This should not affect our meta entity which belongs to
    // the different bundle "visit_count". It should still throw a validation
    // error.
    MetaEntityType::load('download_count')->set('mapping', [
      'entity_test' => ['entity_test' => []],
    ])->save();

    $violations = $meta_entity->validate();
    $this->assertCount(1, $violations);
    $this->assertSame("Test entity (entity_test) doesn't qualify to attach Visit count meta data.", $message);

    // Now allow entity_test:entity_test to attach metadata to the "visit_count"
    // meta entity type. This provides the correct mapping, and the constraint
    // violation should be gone.
    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => ['entity_test' => []],
    ])->save();

    $violations = $meta_entity->validate();
    $this->assertCount(0, $violations);
  }

}
