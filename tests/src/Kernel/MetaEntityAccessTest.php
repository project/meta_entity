<?php

declare(strict_types=1);

namespace Drupal\Tests\meta_entity\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\meta_entity\Entity\MetaEntity;
use Drupal\meta_entity\Entity\MetaEntityType;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\Role;

/**
 * Tests the meta-entity access handler.
 *
 * @group meta_entity
 */
class MetaEntityAccessTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dynamic_entity_reference',
    'entity_test',
    'field',
    'meta_entity',
    'meta_entity_test',
    'system',
    'user',
  ];

  /**
   * Tests the meta-entity access handler.
   */
  public function testAccess(): void {
    $this->installConfig(['meta_entity_test']);
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('meta_entity');
    $this->installEntitySchema('user');

    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => [
        'entity_test' => [],
      ],
    ])->save();

    $meta_entity = MetaEntity::create([
      'type' => 'visit_count',
      'target' => EntityTest::create([
        'type' => 'entity_test',
        'name' => $this->randomString(),
      ]),
    ]);
    $meta_entity->save();

    /** @var \Drupal\user\RoleInterface $role */
    $role = Role::create([
      'id' => $this->randomMachineName(),
      'label' => $this->randomString(),
    ]);
    $role->save();

    // User account with no permissions.
    $account = $this->createUser([], NULL, FALSE, [
      'roles' => [$role],
      // Force the 'uid' to 2 to avoid creating the superuser.
      'uid' => 2,
    ]);

    // The user has no access.
    $this->assertFalse($meta_entity->access('create', $account));
    $this->assertFalse($meta_entity->access('view', $account));
    $this->assertFalse($meta_entity->access('update', $account));
    $this->assertFalse($meta_entity->access('delete', $account));

    $role
      ->grantPermission('create visit_count meta-entity')
      ->grantPermission('view visit_count meta-entity')
      ->grantPermission('update visit_count meta-entity')
      ->grantPermission('delete visit_count meta-entity')
      ->save();

    // Reset internal static cache and test again.
    $this->container->get('entity_type.manager')->getAccessControlHandler('meta_entity')->resetCache();
    $this->assertTrue($meta_entity->access('create', $account));
    $this->assertTrue($meta_entity->access('view', $account));
    $this->assertTrue($meta_entity->access('update', $account));
    $this->assertTrue($meta_entity->access('delete', $account));

    // Revoke all previous permissions but grant 'administer meta entity'.
    $role
      ->revokePermission('create visit_count meta-entity')
      ->revokePermission('view visit_count meta-entity')
      ->revokePermission('update visit_count meta-entity')
      ->revokePermission('delete visit_count meta-entity')
      ->grantPermission('administer meta entity')
      ->save();

    // Reset internal static cache and test again.
    $this->container->get('entity_type.manager')->getAccessControlHandler('meta_entity')->resetCache();
    $this->assertTrue($meta_entity->access('create', $account));
    $this->assertTrue($meta_entity->access('view', $account));
    $this->assertTrue($meta_entity->access('update', $account));
    $this->assertTrue($meta_entity->access('delete', $account));
  }

}
