<?php

declare(strict_types=1);

namespace Drupal\Tests\meta_entity;

use Drupal\KernelTests\KernelTestBase;
use Drupal\meta_entity\Entity\MetaEntityType;

/**
 * Tests the 'meta_entity.repository' service.
 *
 * @group meta_entity
 * @coversDefaultClass \Drupal\meta_entity\MetaEntityRepository
 */
class MetaEntityRepositoryTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'field',
    'meta_entity',
    'meta_entity_test',
    'node',
    'taxonomy',
    'user',
  ];

  /**
   * @covers ::getMetaEntityTypesForBundle
   */
  public function testGetMetaEntityTypesForBundle(): void {
    $this->installConfig(['meta_entity_test']);
    $cache_backend = $this->container->get('cache.meta_entity.repository.memory');

    // Add some non-entity bundles to 'entity_test'.
    $this->container->get('state')->set('entity_test.bundles', [
      'foo' => ['label' => 'Foo'],
      'bar' => ['label' => 'Bar'],
    ]);
    $this->container->get('entity_type.bundle.info')->clearCachedBundles();

    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => ['foo' => []],
      'node' => ['page' => []],
      'user' => ['user' => []],
    ])->save();
    MetaEntityType::load('download_count')->set('mapping', [
      'entity_test' => ['bar' => []],
      'node' => ['article' => []],
    ])->save();
    $service = $this->container->get('meta_entity.repository');

    // Check that there are no cached results.
    $this->assertFalse($cache_backend->get('types:entity_test:foo'));

    $types = $service->getMetaEntityTypesForBundle('entity_test', 'foo');
    $this->assertSame(['visit_count'], $types);

    // Check that the cache entry has been created.
    $cache = $cache_backend->get('types:entity_test:foo');
    $this->assertSame(['visit_count'], $cache->data);
    $this->assertEqualsCanonicalizing([
      'config:meta_entity.type.download_count',
      'config:meta_entity.type.visit_count',
      'config:meta_entity_type_list',
    ], $cache->tags);

    $types = $service->getMetaEntityTypesForBundle('entity_test', 'bar');
    $this->assertSame(['download_count'], $types);

    $types = $service->getMetaEntityTypesForBundle('node', 'page');
    $this->assertSame(['visit_count'], $types);

    $types = $service->getMetaEntityTypesForBundle('node', 'article');
    $this->assertSame(['download_count'], $types);

    $types = $service->getMetaEntityTypesForBundle('user');
    $this->assertSame(['visit_count'], $types);

    // Not mapped.
    $types = $service->getMetaEntityTypesForBundle('taxonomy_term', 'foo');
    $this->assertEmpty($types);
  }

}
