<?php

declare(strict_types=1);

namespace Drupal\Tests\meta_entity\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\meta_entity\Entity\MetaEntityType;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Tests the 'meta_entity_type' dependency management.
 *
 * @group meta_entity
 * @coversDefaultClass \Drupal\meta_entity\Entity\MetaEntityType
 */
class MetaEntityTypeDependencyTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'meta_entity',
    'meta_entity_test',
    'system',
    'taxonomy',
    'text',
  ];

  /**
   * @covers ::calculateDependencies
   * @covers ::onDependencyRemoval
   */
  public function testDependencies(): void {
    $module_installer = $this->container->get('module_installer');
    // Install 'entity_test' via installer so that we can uninstall later.
    $module_installer->install(['entity_test']);

    $this->installConfig(['meta_entity_test']);
    $this->installEntitySchema('taxonomy_term');

    Vocabulary::create(['vid' => 'vocab1'])->save();
    Vocabulary::create(['vid' => 'vocab2'])->save();
    Vocabulary::create(['vid' => 'vocab3'])->save();

    $meta_entity_type = MetaEntityType::load('visit_count');
    $meta_entity_type->set('mapping', [
      'entity_test' => [
        'entity_test' => [
          'field_name' => 'reverse_ref',
        ],
      ],
      // Pick-up only one bundle.
      'taxonomy_term' => ['vocab1' => []],
    ])->save();

    $this->assertSame([
      'config' => [
        'taxonomy.vocabulary.vocab1',
      ],
      'module' => [
        'entity_test',
        'taxonomy',
      ],
    ], $meta_entity_type->getDependencies());

    $meta_entity_type->set('mapping', [
      'entity_test' => [
        'entity_test' => [
          'field_name' => 'reverse_ref',
        ],
      ],
      // Pick-up all bundles explicitly.
      'taxonomy_term' => [
        'vocab1' => [],
        'vocab2' => [],
        'vocab3' => [],
      ],
    ])->save();

    $this->assertSame([
      'config' => [
        'taxonomy.vocabulary.vocab1',
        'taxonomy.vocabulary.vocab2',
        'taxonomy.vocabulary.vocab3',
      ],
      'module' => [
        'entity_test',
        'taxonomy',
      ],
    ], $meta_entity_type->getDependencies());

    // Remove a vocabulary and reload the meta entity type.
    Vocabulary::load('vocab2')->delete();
    $meta_entity_type = MetaEntityType::load('visit_count');

    // Check that the entity has been updated.
    $this->assertSame([
      'config' => [
        'taxonomy.vocabulary.vocab1',
        'taxonomy.vocabulary.vocab3',
      ],
      'module' => [
        'entity_test',
        'taxonomy',
      ],
    ], $meta_entity_type->getDependencies());

    // Remove remaining vocabularies and reload the meta entity type.
    Vocabulary::load('vocab1')->delete();
    Vocabulary::load('vocab3')->delete();
    $meta_entity_type = MetaEntityType::load('visit_count');

    // Check that the entity has been updated.
    $this->assertSame([
      'module' => [
        'entity_test',
      ],
    ], $meta_entity_type->getDependencies());

    // Add back one vocabulary.
    Vocabulary::create(['vid' => 'vocab1'])->save();
    $mapping = $meta_entity_type->get('mapping');
    $mapping['taxonomy_term'] = [
      'vocab1' => [
        'field_name' => 'reverse_reference',
      ],
    ];
    $meta_entity_type->set('mapping', $mapping)->save();

    // Check that the entity dependencies were updated.
    $this->assertSame([
      'config' => [
        'taxonomy.vocabulary.vocab1',
      ],
      'module' => [
        'entity_test',
        'taxonomy',
      ],
    ], $meta_entity_type->getDependencies());

    // Uninstall the Taxonomy module and reload the meta entity type.
    $module_installer->uninstall(['taxonomy']);
    $meta_entity_type = MetaEntityType::load('visit_count');

    // Check that the entity has been updated.
    $this->assertSame([
      'module' => [
        'entity_test',
      ],
    ], $meta_entity_type->getDependencies());

    // Uninstall the Entity Test module and reload the meta entity type.
    $module_installer->uninstall(['entity_test']);
    $meta_entity_type = MetaEntityType::load('visit_count');
    $this->assertEmpty($meta_entity_type->getDependencies());
  }

}
