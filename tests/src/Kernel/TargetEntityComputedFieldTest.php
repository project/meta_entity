<?php

declare(strict_types=1);

namespace Drupal\Tests\meta_entity\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\entity_test\Entity\EntityTestStringId;
use Drupal\KernelTests\KernelTestBase;
use Drupal\meta_entity\Entity\MetaEntity;
use Drupal\meta_entity\Entity\MetaEntityInterface;
use Drupal\meta_entity\Entity\MetaEntityType;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Tests the target entity computed fields.
 *
 * @coversDefaultClass \Drupal\meta_entity\MetaEntityReverseReferenceItemList
 * @group meta_entity
 */
class TargetEntityComputedFieldTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dynamic_entity_reference',
    'entity_test',
    'field',
    'meta_entity',
    'meta_entity_test',
    'taxonomy',
    'text',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('entity_test');
    $this->installConfig(['meta_entity_test']);
    $this->installEntitySchema('meta_entity');
  }

  /**
   * @covers ::computeValue
   */
  public function testComputedFields(): void {
    $this->installEntitySchema('taxonomy_term');

    // Add some non-entity bundles to 'entity_test'.
    $this->container->get('state')->set('entity_test.bundles', [
      'foo' => ['label' => 'Foo'],
      'bar' => ['label' => 'Bar'],
    ]);
    $this->container->get('entity_type.bundle.info')->clearCachedBundles();

    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => ['foo' => ['field_name' => 'visits']],
      'taxonomy_term' => ['vocab1' => ['field_name' => 'usage']],
    ])->save();
    MetaEntityType::load('download_count')->set('mapping', [
      'entity_test' => ['foo' => ['field_name' => 'unique_visits']],
    ])->save();
    Vocabulary::create(['vid' => 'vocab1'])->save();
    Vocabulary::create(['vid' => 'vocab2'])->save();

    $entity_test1 = EntityTest::create([
      'type' => 'foo',
      'name' => 'Entity 1',
    ]);
    $entity_test1->save();
    $meta_entity1 = MetaEntity::create([
      'type' => 'download_count',
      'target' => $entity_test1,
    ]);
    $meta_entity1->save();
    $meta_entity2 = MetaEntity::create([
      'type' => 'visit_count',
      'target' => $entity_test1,
    ]);
    $meta_entity2->save();
    $entity_test2 = EntityTest::create([
      'type' => 'bar',
      'name' => 'Entity 2',
    ]);
    $entity_test2->save();

    $term1 = Term::create([
      'vid' => 'vocab1',
      'name' => 'Term 1',
    ]);
    $term1->save();
    $meta_entity3 = MetaEntity::create([
      'type' => 'visit_count',
      'target' => $term1,
    ]);
    $meta_entity3->save();
    $term2 = Term::create([
      'vid' => 'vocab2',
      'name' => 'Term 2',
    ]);
    $term2->save();

    // Check that the field 'visits' exists only on entity_test > foo.
    $this->assertTrue($entity_test1->hasField('visits'));
    $this->assertFalse($entity_test2->hasField('visits'));
    $this->assertFalse($term1->hasField('visits'));
    $this->assertFalse($term2->hasField('visits'));
    // Check that the field 'unique_visits' exists only on entity_test > foo.
    $this->assertTrue($entity_test1->hasField('unique_visits'));
    $this->assertFalse($entity_test2->hasField('unique_visits'));
    $this->assertFalse($term1->hasField('unique_visits'));
    $this->assertFalse($term2->hasField('unique_visits'));
    // Check that the field 'usage' exists only on taxonomy_term > vocab1.
    $this->assertFalse($entity_test1->hasField('usage'));
    $this->assertFalse($entity_test2->hasField('usage'));
    $this->assertTrue($term1->hasField('usage'));
    $this->assertFalse($term2->hasField('usage'));

    // Check how reverse reference field are computed.
    $this->assertEquals($meta_entity1->id(), $entity_test1->get('unique_visits')->target_id);
    $this->assertEquals($meta_entity2->id(), $entity_test1->get('visits')->target_id);
    $this->assertEquals($meta_entity3->id(), $term1->get('usage')->target_id);

    // Check the caching of 'meta_entity.repository'. The computed field uses
    // this service to retrieve a list of meta entities for a given target
    // entity and is caching the results.
    $cache_backend = $this->container->get('cache.meta_entity.repository.memory');
    $cache = $cache_backend->get("meta:entity_test:{$entity_test1->id()}");
    $this->assertInstanceOf(MetaEntityInterface::class, $cache->data[1]);
    $this->assertEquals($meta_entity1->id(), $cache->data[1]->id());
    $cache = $cache_backend->get("meta:entity_test:{$entity_test1->id()}");
    $this->assertInstanceOf(MetaEntityInterface::class, $cache->data[2]);
    $this->assertEquals($meta_entity2->id(), $cache->data[2]->id());

    // Replace 'vocab1' with 'vocab2'.
    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => ['foo' => ['field_name' => 'visits']],
      'taxonomy_term' => ['vocab2' => ['field_name' => 'usage']],
    ])->save();

    // Check that the field has been removed from $term1 but added to $term2.
    $term1 = Term::load($term1->id());
    $term2 = Term::load($term2->id());
    $this->assertFalse($term1->hasField('usage'));
    $this->assertTrue($term2->hasField('usage'));
  }

  /**
   * @covers \Drupal\meta_entity\MetaEntityReverseReferenceItemList::postSave
   */
  public function testAssignValueToComputedField(): void {
    MetaEntityType::load('download_count')->set('mapping', [
      'entity_test' => ['entity_test' => ['field_name' => 'downloads']],
      'entity_test_string_id' => [
        'entity_test_string_id' => [
          'field_name' => 'downloads',
        ],
      ],
    ])->save();
    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => [
        'entity_test' => [
          'field_name' => 'visits',
        ],
      ],
      'entity_test_string_id' => [
        'entity_test_string_id' => [
          'field_name' => 'visits',
        ],
      ],
    ])->save();

    // Check assigning a meta entity on target entity creation.
    $entity1 = EntityTest::create([
      'type' => 'entity_test',
      'name' => $this->randomString(),
      'visits' => MetaEntity::create([
        'type' => 'visit_count',
        'count' => 100,
      ]),
      'downloads' => MetaEntity::create([
        'type' => 'download_count',
      ]),
    ]);
    $entity1->save();
    $download_count = $entity1->get('downloads')->entity;
    $visit_count = $entity1->get('visits')->entity;
    $this->assertSame('entity_test', $download_count->target->target_type);
    $this->assertSame($entity1->id(), $download_count->target->target_id);
    $this->assertSame('entity_test', $visit_count->target->target_type);
    $this->assertSame($entity1->id(), $visit_count->target->target_id);
    $this->assertEquals(100, $visit_count->count->value);

    // Check assigning a meta entity after the target entity has been saved.
    $entity2 = EntityTest::create([
      'type' => 'entity_test',
      'name' => $this->randomString(),
    ]);
    $entity2->save();
    $entity2->set('downloads', MetaEntity::create([
      'type' => 'download_count',
    ]));
    $entity2->set('visits', MetaEntity::create([
      'type' => 'visit_count',
      'count' => 300,
    ]));
    $entity2->save();
    $download_count = $entity2->get('downloads')->entity;
    $visit_count = $entity2->get('visits')->entity;
    $this->assertSame('entity_test', $download_count->target->target_type);
    $this->assertSame($entity2->id(), $download_count->target->target_id);
    $this->assertSame('entity_test', $visit_count->target->target_type);
    $this->assertSame($entity2->id(), $visit_count->target->target_id);
    $this->assertEquals(300, $visit_count->count->value);

    // Check assigning a meta entity on target entity creation with ID set.
    $entity3 = EntityTest::create([
      'type' => 'entity_test',
      // The ID is set on creation.
      'id' => 10000,
      'name' => $this->randomString(),
      'visits' => MetaEntity::create([
        'type' => 'visit_count',
        'count' => 400,
      ]),
    ]);
    $entity3->save();
    $visit_count = $entity3->visits->entity;
    $this->assertSame('entity_test', $visit_count->target->target_type);
    $this->assertSame($entity3->id(), $visit_count->target->target_id);
    $this->assertEquals(400, $visit_count->count->value);
  }

  /**
   * Test assigning a value to a computed field for an entity using a string ID.
   *
   * This requires version 4 of the dynamic entity reference module which has
   * support for string IDs.
   *
   * @covers \Drupal\meta_entity\MetaEntityReverseReferenceItemList::postSave
   * @group dynamic-entity-reference-4
   */
  public function testAssignValueToComputedFieldForEntityUsingStringId(): void {
    MetaEntityType::load('download_count')->set('mapping', [
      'entity_test' => ['entity_test' => ['field_name' => 'downloads']],
      'entity_test_string_id' => [
        'entity_test_string_id' => [
          'field_name' => 'downloads',
        ],
      ],
    ])->save();
    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => [
        'entity_test' => [
          'field_name' => 'visits',
        ],
      ],
      'entity_test_string_id' => [
        'entity_test_string_id' => [
          'field_name' => 'visits',
        ],
      ],
    ])->save();

    // Check assigning a meta entity on target entity creation.
    $this->installEntitySchema('entity_test_string_id');
    $entity = EntityTestStringId::create([
      'type' => 'entity_test_string_id',
      'id' => $this->randomMachineName(),
      'name' => $this->randomString(),
      'visits' => MetaEntity::create([
        'type' => 'visit_count',
        'count' => 500,
      ]),
      'downloads' => MetaEntity::create([
        'type' => 'download_count',
      ]),
    ]);
    $entity->save();
    $download_count = $entity->get('downloads')->entity;
    $visit_count = $entity->get('visits')->entity;
    $this->assertSame('entity_test_string_id', $download_count->target->target_type);
    $this->assertSame($entity->id(), $download_count->target->target_id);
    $this->assertSame('entity_test_string_id', $visit_count->target->target_type);
    $this->assertSame($entity->id(), $visit_count->target->target_id);
    $this->assertEquals(500, $visit_count->count->value);
  }

}
