<?php

declare(strict_types=1);

namespace Drupal\Tests\meta_entity\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\meta_entity\Entity\MetaEntity;
use Drupal\meta_entity\Entity\MetaEntityType;

/**
 * Tests the dependency between target and meta entities.
 *
 * @group meta_entity
 */
class TargetEntityDependencyTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dynamic_entity_reference',
    'entity_test',
    'field',
    'meta_entity',
    'meta_entity_test',
    'user',
  ];

  /**
   * Tests the meta entity auto-removal when the target entity is removed.
   *
   * @see \meta_entity_entity_delete()
   */
  public function testMetaEntityRemovalOnTargetRemoval(): void {
    $repository = $this->container->get('meta_entity.repository');
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('meta_entity');
    $this->installConfig(['meta_entity_test']);

    MetaEntityType::load('download_count')->set('mapping', [
      'entity_test' => ['entity_test' => []],
    ])->save();
    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => ['entity_test' => []],
    ])->save();

    $entity = EntityTest::create([
      'type' => 'entity_test',
      'name' => $this->randomString(),
    ]);
    $entity->save();

    $meta_entity1 = MetaEntity::create([
      'type' => 'download_count',
      'target' => $entity,
    ]);
    $meta_entity1->save();

    // Check that 'meta_entity.repository' returns the correct entries.
    $meta_entities = $repository->getMetaEntitiesForEntity($entity);
    $this->assertEquals($meta_entity1->id(), $meta_entities[$meta_entity1->id()]->id());

    $meta_entity2 = MetaEntity::create([
      'type' => 'visit_count',
      'target' => $entity,
    ]);
    $meta_entity2->save();
    $meta_entities = $repository->getMetaEntitiesForEntity($entity);
    $this->assertEquals($meta_entity2->id(), $meta_entities[$meta_entity2->id()]->id());

    // Delete the target entity.
    $entity->delete();

    $this->assertEmpty($repository->getMetaEntitiesForEntity($entity));
    $this->assertNull(MetaEntity::load($meta_entity1->id()));
    $this->assertNull(MetaEntity::load($meta_entity2->id()));
  }

}
