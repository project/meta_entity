<?php

declare(strict_types=1);

namespace Drupal\Tests\meta_entity\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\meta_entity\Entity\MetaEntity;
use Drupal\meta_entity\Entity\MetaEntityType;

/**
 * Tests the 'UniquePerMetaTypeAndTarget' constraint.
 *
 * @group meta_entity
 * @coversDefaultClass \Drupal\meta_entity\Plugin\Validation\Constraint\UniquePerMetaTypeAndTargetValidator
 */
class UniquePerMetaTypeAndTargetConstraintTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dynamic_entity_reference',
    'entity_test',
    'field',
    'meta_entity',
    'meta_entity_test',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('meta_entity');
    $this->installConfig(['meta_entity_test']);
    MetaEntityType::load('visit_count')->set('mapping', [
      'entity_test' => ['entity_test' => []],
    ])->save();
    MetaEntityType::load('download_count')->set('mapping', [
      'entity_test' => ['entity_test' => []],
    ])->save();
  }

  /**
   * @covers ::validate
   */
  public function testConstraint(): void {
    $entity = EntityTest::create([
      'type' => 'entity_test',
      'name' => 'Just an innocent target',
    ]);
    $entity->save();

    $meta_entity = MetaEntity::create([
      'type' => 'visit_count',
      'count' => 30,
      'target' => $entity,
    ]);
    $violations = $meta_entity->validate();

    // Check that the first meta entity of a given type validates.
    $this->assertCount(0, $violations);

    // Save the meta entity and create a new of the same type targeting the same
    // content entity.
    $meta_entity->save();

    $meta_entity2 = MetaEntity::create([
      'type' => 'visit_count',
      'count' => 60,
      'target' => $entity,
    ]);

    $violations = $meta_entity2->validate();

    // Check that the second meta entity doesn't validate.
    $this->assertCount(1, $violations);
    $message = strip_tags((string) $violations[0]->getMessage());
    $this->assertSame("Just an innocent target test entity already has attached Visit count meta data.", $message);

    // Create a new target entity.
    $other_entity = EntityTest::create([
      'type' => 'entity_test',
      'name' => 'Other target',
    ]);
    $other_entity->save();

    $same_as_meta_entity2_but_different_target = MetaEntity::create([
      'type' => 'visit_count',
      'count' => 60,
      'target' => $other_entity,
    ]);

    $violations = $same_as_meta_entity2_but_different_target->validate();

    // Check that the third meta entity validates.
    $this->assertCount(0, $violations);

    $same_as_meta_entity2_but_different_type = MetaEntity::create([
      'type' => 'download_count',
      'count' => 60,
      'target' => $entity,
    ]);

    $violations = $same_as_meta_entity2_but_different_type->validate();

    // Check that the third meta entity validates.
    $this->assertCount(0, $violations);
  }

}
